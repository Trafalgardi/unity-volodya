using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class Target : MonoBehaviour, IPointerClickHandler
{
    public Rigidbody TargetRb;
    public GameManager GameManager;

    public float MinSpeed = 12;
    public float MaxSpeed = 16;
    public float MaxTorque = 10;
    public float XRange = 9;
    public float YSpawnPos = 5;
    public float ZSpawnPos = -2;
    public float YPositionDestroy = -7;

    public int PointValue;
    public ParticleSystem ExplosionParticle;
    void Start()
    {
        XRange = Screen.width / 100;
        YSpawnPos = Screen.height / 100;
        TargetRb.AddForce(RandomForce(), ForceMode.Force);
        TargetRb.AddTorque(RandomTorque(), RandomTorque(),RandomTorque(), ForceMode.Impulse);

        transform.position = RandomSpawnPos();
    }
    
    // Destroy если звезда под экраном СДЕЛАТЬ ЧЕРЕЗ КОЛЛАЙДЕР
    void Update()
    {
        if (transform.position.y < YPositionDestroy)
        {
            Destroy(gameObject);
        }
    }
    // Нажатие на звезду (звук, эффект Poof, +1 к счётчику) ЗВУК ДОБАВИТЬ В UpdateCount;
    public void OnMouseDown()
    {
        if (GameManager.IsGameActive)
        {
            GameManager.IfDestroyed();
            Destroy(gameObject);
            GameManager.UpdateCount(PointValue);
            Instantiate(ExplosionParticle, transform.position, ExplosionParticle.transform.rotation);
        }
    }
    

    private Vector3 RandomForce()
    {
        return Vector3.down * Random.Range(MinSpeed, MaxSpeed);
    }

    private float RandomTorque()
    {
        return Random.Range(-MaxTorque, MaxTorque);
    }

    private Vector3 RandomSpawnPos()
    {
        return new Vector3(Random.Range(-XRange, XRange), YSpawnPos, ZSpawnPos);
    }
// Посмотреть и сделать вместо OnMouseDown; 
    public void OnPointerClick(PointerEventData eventData)
    {
        throw new NotImplementedException();
    }
}
