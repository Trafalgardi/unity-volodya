using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGame : MonoBehaviour
{
    public Button Button;
    void Start()
    {
        Button.onClick.AddListener(ExitGameFunction);
    }
    private void ExitGameFunction()
    {
        Application.Quit();
    }
}
