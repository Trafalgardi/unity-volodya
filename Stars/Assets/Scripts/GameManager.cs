using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
    public AudioSource PlayerAudio;
    public AudioClip DestroySound;
    
    public Target TargetScript;

    public GameObject TimerText;
    public TextMeshProUGUI Timer;
    public GameObject CountText;
    public TextMeshProUGUI Count;
    public Button TryAgainButton;
    public GameObject StartButton;
    public Button Complete;

    public float SpawnRate = 1.0f;
    private int _score;
    public bool IsGameActive;
    private bool ActiveTimer = false;
    private float _timeRemaning = 10.0f;
    
    // Работа таймера
    private void Update()
    {
        if (_timeRemaning > 0 && IsGameActive && ActiveTimer)
        {
            _timeRemaning -= Time.deltaTime;
            Timer.text = "Time: " + Mathf.RoundToInt(_timeRemaning);
        }
        else if (_timeRemaning <= 0 && IsGameActive)
        {
            GameOver();
        }
    }
    
    // Спаун звёзд, пока игра активна
    IEnumerator SpawnTarget()
    {
        while(IsGameActive)
        {
            yield return new WaitForSeconds(SpawnRate);
            var target = Instantiate(TargetScript);
            target.GameManager = this;
        }
    }
    
    // Счётчик собранных звёзд
    public void UpdateCount(int countToAdd)
    {
        _score += countToAdd;
        Count.text = "Count: " + _score;
        if (_score >= 10)
        {
            GameOver();
        }
    }
    
    // Игра окончится, если собранно 10 звёзд или прошло 10 секунд
    public void GameOver()
    {
        TryAgainButton.gameObject.SetActive(true);
        Complete.gameObject.SetActive(false);
        IsGameActive = false;
    }
    
    // Кнопка TryAgain, перезапускающая сцену
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    // Запуск игры, в зависимости от выбранного режима (True без таймера, False с таймером)
    public void StartGame()
    {
        if (DataHolder.GameMode)
        {
            IsGameActive = true;
            _score = 0;

            StartCoroutine(SpawnTarget());
            UpdateCount(0);
        
            StartButton.gameObject.SetActive(false);
            CountText.gameObject.SetActive(true);
            Complete.gameObject.SetActive(true);
        }
        else
        {
            ActiveTimer = true;
            IsGameActive = true;
            _score = 0;

            StartCoroutine(SpawnTarget());
            UpdateCount(0);
        
            StartButton.gameObject.SetActive(false);
            CountText.gameObject.SetActive(true);
            TimerText.gameObject.SetActive(true);
            Complete.gameObject.SetActive(true);
        }
    }
    
    // Звук при сборе звезды
    public void IfDestroyed()
    {
        PlayerAudio.PlayOneShot(DestroySound);
    }
    
}
