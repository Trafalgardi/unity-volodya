using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SaveUserName : MonoBehaviour
{
    public TMP_InputField namePan;

    private void Start()
    {
        namePan.onEndEdit.AddListener(SetName);
        if (PlayerPrefs.HasKey("Name"))
        {
            namePan.text = PlayerPrefs.GetString("Name");
            Debug.Log("Load: ");
        }
    }

    private void SetName (string name)
    {
        PlayerPrefs.SetString("Name", name);
        Debug.Log("Save " + name);
    }
}
