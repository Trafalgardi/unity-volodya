using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameMode : MonoBehaviour
{
    public Button Button;

    public bool ChoseGameMode;
    void Start()
    {
        Button.onClick.AddListener(SetGameMode);
    }
    
// Устанавливает режим игры и переходит на следующую сцену
    private void SetGameMode()
    {
        DataHolder.GameMode = ChoseGameMode;
        SceneManager.LoadScene("Game");
    }
}
