﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsListElement : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private TMP_Text buttonText;
    [SerializeField] private TMP_Text buttonAssignment;
    [SerializeField] private TMP_Text errorText;

    public event Action ButtonPressed;
    public event Action<string, KeyCode> ButtonChanged;
    public event Func<KeyCode, bool> ButtonBusy; 
    
    private IEnumerator _coroutine;
    private string _tempTextKey;
    private KeyCode _key;

    private void OnEnable()
    {
        button.onClick.AddListener(ChangeKeyAssignment);
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(ChangeKeyAssignment);
    }

    public void Init(string assignment, KeyCode key)
    {
        _key = key;
        buttonAssignment.text = assignment;

        buttonText.text = _key.ToString();
    }

    private void ChangeKeyAssignment()
    {
        _tempTextKey = buttonText.text;
        buttonText.text = "???";
        _coroutine = Wait();
        ButtonPressed?.Invoke();
        StartCoroutine(_coroutine);
    }
    private IEnumerator Wait()
    {
        while(true)
        {
            yield return null;

            if(Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
            {
                ButtonPressed?.Invoke();
                buttonText.text = _tempTextKey;
                errorText.text = "";
                StopCoroutine(_coroutine);
            }
            else if (Input.anyKeyDown)
            {
                foreach(KeyCode k in Enum.GetValues(typeof(KeyCode)))
                {
                    if(ButtonIsBusy(k) == false && Input.GetKeyDown(k) == true)
                    {
                        ButtonPressed?.Invoke();
                        ButtonChanged?.Invoke(buttonAssignment.text, k);
                        buttonText.text = k.ToString();
                        errorText.text = "";
                        StopCoroutine(_coroutine);
                        break;
                    }
                }
            }
        }
    }

    private bool ButtonIsBusy(KeyCode key)
    {
        if (ButtonBusy?.Invoke(key) == false)
            return false;
        errorText.text = "Кнопка уже занята";
        return true;
    }
}