﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Classes
{
    public class SaveSettingsSystem
    {
        private static readonly string INPUT_SETTINGS_PATH = Application.persistentDataPath + "/inputSettings.json";
    
        public Dictionary<string, KeyCode> GetInputSettings()
        {
            if (File.Exists(INPUT_SETTINGS_PATH))
            {
                var result = File.ReadAllText(INPUT_SETTINGS_PATH);
                if(result.Length <= 0)
                    return SetDefaultInputSettings();
                var settings = JsonConvert.DeserializeObject<Dictionary<string, KeyCode>>(result);
                return settings;
            }
            else
            {
                return SetDefaultInputSettings();
            }
        }
    
        public void SaveInputSettings(Dictionary<string, KeyCode> settings)
        {
            if (File.Exists(INPUT_SETTINGS_PATH))
            {
                var json = JsonConvert.SerializeObject(settings);
                File.WriteAllText(INPUT_SETTINGS_PATH, json);
            }
            else
            {
                SetDefaultInputSettings();
                var json = JsonConvert.SerializeObject(settings);
                File.WriteAllText(INPUT_SETTINGS_PATH, json);
            }
        }
    
        public Dictionary<string, KeyCode> SetDefaultInputSettings()
        {
            var settings = new Dictionary<string, KeyCode>()
            {
                {"Forward", KeyCode.W},
                {"Back", KeyCode.S},
                {"Left", KeyCode.A},
                {"Right", KeyCode.D},
                {"Set Color 1", KeyCode.Alpha1},
                {"Set Color 2", KeyCode.Alpha2},
                {"Set Color 3", KeyCode.Alpha3},
                {"Set Color 4", KeyCode.Alpha4},
                {"Rotate left", KeyCode.Q},
                {"Rotate right", KeyCode.E},
                {"Increase in size", KeyCode.Mouse0},
                {"Decrease in size", KeyCode.Mouse1},
            };

            return settings;
        }
    }
}