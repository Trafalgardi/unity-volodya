﻿using System.Collections.Generic;
using Classes;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private PauseScreen pauseScreen;

    private Dictionary<string, KeyCode> _inputSettings;

    private SaveSettingsSystem _saveSettingsSystem;

    private static InputManager _instance;

    public bool IsPaused { get; private set; }
    public bool BlockEscButton { get; private set; }

    private void Awake()
    {
        SingletonRealization();
        _saveSettingsSystem = new SaveSettingsSystem();
        _inputSettings = _saveSettingsSystem.GetInputSettings();
    }

    private void Start()
    {
        pauseScreen.CreateButtons(_inputSettings);
        IsPaused = false;
        BlockEscButton = false;
    }

    private void SingletonRealization()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void ChangeStateEscButton() => BlockEscButton = BlockEscButton == false;

    public void SetDefaultSettings()
    {
        _inputSettings = _saveSettingsSystem.SetDefaultInputSettings();
        _saveSettingsSystem.SaveInputSettings(_inputSettings);
        pauseScreen.RefreshButtons(_inputSettings);
    }

    public void ChangeKey(string keyName, KeyCode key)
    {
        _inputSettings[keyName] = key;
        _saveSettingsSystem.SaveInputSettings(_inputSettings);
    }

    public bool KeyAssigned(KeyCode key)
    {
        bool keyBusy = false;

        foreach (var keyCode in _inputSettings)
        {
            if (keyCode.Value == key)
            {
                keyBusy = true;
            }
        }

        return keyBusy;
    }

    public void Paused()
    {
        if (IsPaused == false)
        {
            IsPaused = true;
            Time.timeScale = 0;
            pauseScreen.gameObject.SetActive(true);
        }
        else
        {
            IsPaused = false;
            Time.timeScale = 1;
            pauseScreen.gameObject.SetActive(false);
        }
    }

    public KeyCode GetKeycode(string key)
    {
        return _inputSettings[key];
    }
}