﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScreen : MonoBehaviour
{
    [SerializeField] private InputManager inputManager;
    [SerializeField] private SettingsListElement settingsListElement;
    [SerializeField] private Transform settingsList;
    [SerializeField] private Button defaultSettingsButton;

    private void OnEnable()
    {
        defaultSettingsButton.onClick.AddListener(inputManager.SetDefaultSettings);
    }

    private void OnDisable()
    {
        defaultSettingsButton.onClick.RemoveListener(inputManager.SetDefaultSettings);
    }
    
    public void CreateButtons(Dictionary<string, KeyCode> inputSettings)
    {
        foreach (var key in inputSettings)
        {
            var button = Instantiate(settingsListElement, settingsList);
            button.Init(key.Key, key.Value);
            button.ButtonPressed += inputManager.ChangeStateEscButton;
            button.ButtonChanged += inputManager.ChangeKey;
            button.ButtonBusy += inputManager.KeyAssigned;
        }
    }

    public void RefreshButtons(Dictionary<string, KeyCode> inputSettings)
    {
        while (settingsList.transform.childCount > 0)
        {
            DestroyImmediate(settingsList.transform.GetChild(0).gameObject);
        }
        
        CreateButtons(inputSettings);
    }
}