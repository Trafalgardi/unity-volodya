using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private InputManager inputManager;
    
    private MeshRenderer _renderer;
    private Camera _gameCamera;

    private float _speed = 10f;
    private float _minScale = 0.5f; 
    private float _maxScale = 3.5f; 
    
    private float _yAngle = 40f;
    private float _xAngle = 0f;
    private float _zAngle = 0f;

    private readonly Vector3 _scaleMultipler = new Vector3(0.5f, 0.5f, 0.5f);

    private void Start()
    {
        _gameCamera = GetComponentInChildren<Camera>();
        _renderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        if (inputManager.IsPaused == false)
        {
            FollowPlayer();
            MovementLogic();
            ColorChangeLogic();
            ScaleLogic();
        }
        PauseSet();
    }

    private void FollowPlayer()
    {
        var forward = _gameCamera.transform.forward;
        forward.y = 0;
        forward.Normalize();
    }

    private void MovementLogic()
    {
        if (Input.GetKey(inputManager.GetKeycode("Forward"))) 
        {
            transform.Translate(Vector3.forward * (_speed * Time.deltaTime));
        }
        if (Input.GetKey(inputManager.GetKeycode("Back"))) 
        {
            transform.Translate(Vector3.back * (_speed * Time.deltaTime));
        }
        if (Input.GetKey(inputManager.GetKeycode("Left"))) 
        {
            transform.Translate(Vector3.left * (_speed * Time.deltaTime));
        }
        if (Input.GetKey(inputManager.GetKeycode("Right"))) 
        {
            transform.Translate(Vector3.right * (_speed * Time.deltaTime));
        }
        
        if (Input.GetKey(inputManager.GetKeycode("Rotate left"))) 
        {
            transform.Rotate(_xAngle, -_yAngle * Time.deltaTime, _zAngle);
        }
        if (Input.GetKey(inputManager.GetKeycode("Rotate right"))) 
        {
            transform.Rotate(_xAngle, _yAngle * Time.deltaTime, _zAngle);
        }
    }

    private void ColorChangeLogic()
    {
        if (Input.GetKeyDown(inputManager.GetKeycode("Set Color 1"))) 
        {
            _renderer.material.color = Color.green;
        }
        if (Input.GetKeyDown(inputManager.GetKeycode("Set Color 2"))) 
        {
            _renderer.material.color = Color.red;
        }
        if (Input.GetKeyDown(inputManager.GetKeycode("Set Color 3"))) 
        {
            _renderer.material.color = Color.blue;
        }
        if (Input.GetKeyDown(inputManager.GetKeycode("Set Color 4"))) 
        {
            _renderer.material.color = Color.yellow;
        }
    }

    private void ScaleLogic()
    {
        if (Input.GetKey(inputManager.GetKeycode("Increase in size"))) 
        {
            if (transform.localScale.x < _maxScale)
            {
                transform.localScale += _scaleMultipler * Time.deltaTime;
            }
        }
        
        if (Input.GetKey(inputManager.GetKeycode("Decrease in size"))) 
        {
            if (transform.localScale.x > _minScale)
            {
                transform.localScale -= _scaleMultipler * Time.deltaTime;
            }
        }
    }

    private void PauseSet()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && inputManager.BlockEscButton == false)
        {
            inputManager.Paused();
        }
    }
}
